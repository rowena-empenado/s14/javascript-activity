/* 
	Console log the message Hello World to ensure that the script file is properly associated with the html file.
*/

	console.log("Hello World");


/*
	Create multiple variables that will store the different JavaScript data types containing information relating to user details.
*/

	let userDetails = {
		firstName : "Ely",
		lastName : "Harrington",
		age : 25,
		hobbies : ["Cooking", "Mountain Climbing", "Swimming", "Coding", "Sleeping"],
		workAddress: {
			houseNumber : "08",
			street : "Diamond St.",
			subdivision : "Golden Subdivision",
			city : "Vancouver",
			country : "Canada",
		}
	}

	console.log("First Name: " + userDetails.firstName);
	console.log("Last Name: " + userDetails.lastName);
	console.log("Age: " + userDetails.age);
	console.log("Hobbies: ");
	console.log(userDetails.hobbies);
	console.log("Work Address: ")
	console.log(userDetails.workAddress);
	console.log(userDetails.firstName + " " + "is " + userDetails.age + " years of age.")


/*
	Create a function named printUserInfo that will accept the following information:
	- First name
	- Last name
	- Age
	The printUserInfo function will print those details in the console as a single string. 
	This function will also print the hobbies and work address.
*/

	function printUserInfo(firstName, lastName, age) {
		console.log("This was printed inside a function.")
		console.log(firstName + ' is ' + age + ' years of age.')
	}
	printUserInfo(userDetails.firstName, userDetails.lastName, userDetails.age)


	function printUserInfo2(hobbies, workAddress) {
		console.log("This was printed inside a function.")
		console.log(hobbies, workAddress)
	}
	printUserInfo2(userDetails.hobbies, "")
	printUserInfo2("", userDetails.workAddress)

